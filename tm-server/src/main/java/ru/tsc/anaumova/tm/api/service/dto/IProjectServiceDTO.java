package ru.tsc.anaumova.tm.api.service.dto;

import ru.tsc.anaumova.tm.dto.model.ProjectDTO;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {
}