package ru.tsc.anaumova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
