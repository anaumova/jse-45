package ru.tsc.anaumova.tm.api.repository.model;

import ru.tsc.anaumova.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}